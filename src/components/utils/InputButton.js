import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';



const InputButton = ( props ) => {
    const useStyles =  makeStyles(theme => ({

        root: {
            '&:hover': props.color ? null : {
                backgroundColor: 'rgba(0, 0, 0, 0.05)'
            },
            marginBottom: theme.spacing(2),
            marginLeft: theme.spacing(1),
            backgroundColor: props.color ? null : "#fff",
            border: '1px solid rgba(0, 0, 0, 0.2)',
            textTransform: 'capitalize',
        },
      }));
    const classes = useStyles();
    return (
        <Button className={ classes.root } disableElevation {...props} />
    )
}


export default InputButton;