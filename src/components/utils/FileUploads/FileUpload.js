import React, {useState} from 'react';
import {useDropzone} from 'react-dropzone'

import { Typography } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperclip, faTimes } from '@fortawesome/free-solid-svg-icons'


import './FileUploads.scss';



const FileUpload = (props) => {
    const [files, setFiles] = useState({});


    const storeFiles = _files => {
        if (Object.keys(files).length > 3)
            // this could use some feedback for the users....
            return;

        const filesObj = _files.reduce((acc, file) => {
            file.path = `${file.path}-${file.lastModified}`;
            acc[file.path] = file;
            return acc;
        }, {});

        console.log('filesObj', filesObj)

        const newFilesVal = {
            ...files,
            ...filesObj
        }
        setFiles(newFilesVal);
    }

    const {
        getRootProps,
        getInputProps,
        rootRef, // Ref to the `<div>`
        inputRef // Ref to the `<input>`
    } = useDropzone({
        accept: 'application/pdf',
        onDrop: storeFiles
      })



    const {ref, ...rootProps} = getRootProps()

    const removeFile = (file) => {
        const newFiles = {...files};
        delete newFiles[file.path];
        setFiles( newFiles );
    }

    const clickBrowse = (e) => {
        e.preventDefault();
        const input = inputRef.current;
        input.click();
    }

    const thumbs = Object.values(files).map(file => {
        return <div
            className="file-uploads--media__previews"
            key={ file.name }
            >
                <Typography variant="caption" className="file-uploads--media__previews__preview__icon" component="div">
                    <FontAwesomeIcon
                        icon={faPaperclip}
                    />
                </Typography>

                <Typography variant="caption" className="file-uploads--media__previews__preview__name" component="div">
                    { file.name }
                </Typography>

                <Typography variant="caption" component="div">
                    <FontAwesomeIcon
                        icon={faTimes}
                        title="Remove"
                        className="remove"
                        onClick={(e) => {
                            e.stopPropagation();
                            removeFile(file);
                        }}
                    />
                </Typography>

        </div>
    });

    return (
        <div {...rootProps}>
            <div className="file-uploads" {...getRootProps()}
                onClick={(e) => {
                    e.stopPropagation();
                }}
            >
                <input {...getInputProps()} onChange={(e) => {
                    e.persist()

                    console.log([...e.target.files]);
                    storeFiles([...e.target.files])
                }} />
                <div onClick={clickBrowse}>
                    <FontAwesomeIcon className="file-uploads__drop-icon"
                        icon={faPaperclip}
                    />

                    <Typography variant="body2" align="center" component="span">
                        Drop up to 3 pds or click to browse
                    </Typography>
                </div>


                { !!Object.values(files).length && <div className="file-uploads__previews">
                        {thumbs}
                    </div>
                }
            </div>
        </div>
    )
}



export default FileUpload;