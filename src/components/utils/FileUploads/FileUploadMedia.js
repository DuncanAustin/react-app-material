import React, {useState, useEffect} from 'react';
import {useDropzone} from 'react-dropzone'

import { Typography, Link, RootRef } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUpload } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'


import './FileUploads.scss';



const FileUploadMedia = (props) => {
    const [files, setFiles] = useState({});

    const storeFiles = _files => {
        const filesObj = _files.reduce((acc, file) => {
            file.preview = URL.createObjectURL(file);
            acc[file.path] = file;
            return acc;
        }, {});

        const newFilesVal = {
            ...files,
            ...filesObj
        }
        setFiles(newFilesVal);
    }

    const {
        getRootProps,
        getInputProps,
        rootRef, // Ref to the `<div>`
        inputRef // Ref to the `<input>`
       } = useDropzone({
        accept: 'image/gif, image/jpeg, image/png',
        onDrop: storeFiles
      })

    const {ref, ...rootProps} = getRootProps()

    const removeImage = (image) => {
        const newFiles = {...files};
        delete newFiles[image.path];
        setFiles( newFiles );
    }

    const clickBrowse = (e) => {
        e.preventDefault();
        const input = inputRef.current;
        input.click();
    }

    const thumbs = Object.values(files).map(file => {
        return <div
            className="file-uploads--media__previews__preview"
            key={ file.path }
            style={{
                backgroundImage: `url(${file.preview})`
            }}>
                <FontAwesomeIcon
                    icon={faTimesCircle}
                    title="Remove"
                    className="remove"
                    onClick={() => {
                        removeImage(file);
                    }}
                />
        </div>
    });


    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        Object.values(files).forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);




    return (
        <RootRef rootRef={ref}>
            <div {...rootProps}>
                <div className="file-uploads file-uploads--media" {...getRootProps()} onClick={(e) => {
                    e.stopPropagation();
                }}>
                    <input {...getInputProps()} />
                    <div className="file-uploads--media__drop-icon">
                        <FontAwesomeIcon icon={faUpload}  />
                        <Typography variant="body2" gutterBottom={true} align="center" component="div">
                            Drop a file here
                        </Typography>
                    </div>

                    <Typography variant="body2" align="center" component="div">
                        Choose from <Link>Media library</Link> or <Link onChange={storeFiles} onClick={clickBrowse}>browse</Link>
                    </Typography>
                    <Typography variant="caption" align="center" component="div">
                        (.jpg, .png, .gif)
                    </Typography>

                    { !!Object.values(files).length && <div className="file-uploads--media__previews">
                            {thumbs}
                        </div>
                    }
                </div>
            </div>
        </RootRef>
    )
}



export default FileUploadMedia;