import React from 'react';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles =  makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(2)
    },
  }));

const InputCheckbox = ( props ) => {
    const classes = useStyles();
    return (
        <FormControlLabel
            className={ classes.root }
            control={
                <Checkbox
                    color="primary"
                    checked = {props.checked}
                    name = { props.name }
                    onChange = {
                        (e) => {
                            console.log('...');
                            props.onChange(e);
                        }
                    }

                    inputProps = { props.inputProps } />
            }
            label={ props.label }
        />
    )
}


export default InputCheckbox;