import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles =  makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(2),
        '& > .MuiFilledInput-root': {
            backgroundColor: 'rgba(0, 3, 79, 0.05)',
        },
        '& > .MuiFilledInput-root:hover': {
            backgroundColor: 'rgba(0, 3, 79, 0.05)',
        }
    },
  }));

const InputText = ( props ) => {
    const classes = useStyles();
    return (
        <TextField className={classes.root} variant="filled"  {...props }></TextField>
    )
}


export default InputText;