import React from 'react';
import {
    Switch,
    Route
  } from "react-router-dom";

import PageHome from '../../pages/home/PageHome';
import PageMobile from '../../pages/mobile/PageMobile';


import './AppBody.scss';

const AppBody = () => {
    return (
        <div className="app-body">
            <Switch>
                <Route exact path="/">
                    <PageHome />
                </Route>

                <Route path="/mobile">
                    <PageMobile />
                </Route>
            </Switch>
        </div>
    )
}


export default AppBody;

/* TODO:
<Route path="/comms">
    <PageComms />
</Route>
<Route path="/mobile">
    <PageMobile />
</Route>
<Route path="/people">
    <PagePeople />
</Route>
<Route path="/reports">
    <PageReports />
</Route>
*/