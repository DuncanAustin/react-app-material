import React from 'react';
import {NavLink} from "react-router-dom";
import TypoGraphy from '@material-ui/core/Typography';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestionCircle, faCog, faUser, faCaretDown } from '@fortawesome/free-solid-svg-icons'

import './NavBar.scss';


const NavBar = () => {

    return (
        <nav className="header-nav">
            <NavLink exact to="/" activeClassName="header-nav__item--active" className="header-nav__item">
                <TypoGraphy color="inherit" variant="inherit">
                    Home
                </TypoGraphy>
            </NavLink>
            <NavLink to="/comms" activeClassName="header-nav__item--active" className="header-nav__item">
                <TypoGraphy color="inherit" variant="inherit">
                    Comms
                </TypoGraphy>
            </NavLink>
            <NavLink to="/mobile" activeClassName="header-nav__item--active" className="header-nav__item">
                <TypoGraphy color="inherit" variant="inherit">
                    Mobile
                </TypoGraphy>
            </NavLink>
            <NavLink to="/people" activeClassName="header-nav__item--active" className="header-nav__item">
                <TypoGraphy color="inherit" variant="inherit">
                    People
                </TypoGraphy>
            </NavLink>
            <NavLink to="/reports" activeClassName="header-nav__item--active" className="header-nav__item">
                <TypoGraphy color="inherit" variant="inherit">
                    Reports
                </TypoGraphy>
            </NavLink>

            <div className="header-nav__secondary">
                <div className="header-nav__item">
                    <TypoGraphy color="inherit" variant="inherit">
                        <FontAwesomeIcon
                            icon={faQuestionCircle}
                        />
                        Help
                        <FontAwesomeIcon
                            icon={faCaretDown}
                        />
                    </TypoGraphy>
                </div>
                <div className="header-nav__item">
                    <TypoGraphy color="inherit" variant="inherit">
                        <FontAwesomeIcon
                            icon={faCog}
                        />
                        Account Name
                        <FontAwesomeIcon
                            icon={faCaretDown}
                        />
                    </TypoGraphy>
                </div>
                <div className="header-nav__item">
                    <TypoGraphy color="inherit" variant="inherit">
                        <FontAwesomeIcon
                            icon={faUser}
                        />
                        Username
                        <FontAwesomeIcon
                            icon={faCaretDown}
                        />
                    </TypoGraphy>
                </div>
            </div>
        </nav>
    )
}

export default NavBar;
