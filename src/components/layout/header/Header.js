import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'
import TypoGraphy from '@material-ui/core/Typography'
import NavBar from './NavBar.js';
import Logo from './Logo';

import './Header.scss';

const Header = () => {
    return (
      <AppBar color="inherit" position="static" >
        <Toolbar className="app-header" variant="dense">
          <div className="app-header__logo">
            <TypoGraphy variant="inherit"
              color="inherit"
            >
              <Logo />
            </TypoGraphy>
          </div>
          <NavBar />
        </Toolbar>
      </AppBar>
    )
}

export default Header;