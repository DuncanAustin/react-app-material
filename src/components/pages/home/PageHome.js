import React from 'react';
import HomeContent from './HomeContent';
import HomeSidebar from './HomeSidebar';

const PageHome = () => {
    return [
        <HomeContent key="page-home-content" />,
        <HomeSidebar  key="page-home-sidebar" />
    ]
}


export default PageHome