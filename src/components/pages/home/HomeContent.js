import React from 'react';
import { Typography } from '@material-ui/core'


const AppContent = () => {
    return (

        <div className="app-content-wrapper__content">
            <Typography variant="h4" component="h2" gutterBottom={true}>
                Duncan's assessment Task
            </Typography>
            <Typography variant="body1">
                The assessment layout is found under Mobile in the top nav.
                <br /><br />
            </Typography>
            <Typography variant="body1">
                I chose to use React and Material-UI because it's what I have more experience with.
                <br /><br />
            </Typography>
            <Typography variant="body1">
                I've tried to structure the code as logically as possible.
                I've seperated some of the Material-UI form components into seperate wrapper components.
                I think it's a good idea to insulate thrid party libs as much as possible so that you're not too reliant on them.
                <br /><br />
                In the real world I wouldn't use it for things like typography and general layout - I prefer to do those from scratch.
                I used them here to get a lot of Material styling out of the box, bit I'm not sure that that samed me time in the end.
                <br /><br />
                I did run out of time at the end, and so the Post Settings and the right hand nav uses some unwrapped material-UI components.
                <br /><br />
                As for styling, I've used a combination of BEM and component-scoped styles. I think both are useful, but I really just wanted to
                demonstrate both.
                <br /><br />
            </Typography>

            <Typography variant="h5" component="h2"  gutterBottom={true}>
                Challenges
            </Typography>
            <Typography variant="body1"  >
                The brief image is at a higher resolution (or perhaps my macbook screen is a lower resolution?) so I found it difficult to judge what the scale should be.
                I looked on the Poppulo web to try to find examples that matched, but the styles there are proportionally different.

                <br /><br />
                In the end, I eyeballed a scale that looked right to me, assuming that the brief image was from a higher res screen than mine.
                <br /><br />
            </Typography>
            <Typography variant="body1"  >
                The file uploads took a suprisingly long - the react-dropzone libs isn't well documented and the small tweaks
                needed to get it like the brief were very fiddly - for example, they by default have the blowse files opening on clikem whereas
                the brief has a link to click to browse. I also hade to do a fair bit of inspecting to figure the image peviews out (not technically
                in the brief, but what's an image upload without previews).
                <br /><br />
            </Typography>
            <Typography variant="body1"  >
                Also, tweaking the Material-UI select and radio components took much longer than I expected, so I hadn't really left enough time to wrestle with them.
                There's wasn't anything particularly difficult, just so many small niggles at every tweak that it took time.
                <br /><br />
            </Typography>
            <Typography variant="body1"  >
                The files state, and the Post Setting stare, are in their own components. This shouldn't be so - I was going to push that up to the PageMobile component but just ran out of time.
                <br /><br />
            </Typography>
            <Typography variant="body1"  >
                Customing Material-UI was very frustrating, largely because I was pressured for time that I didn't have to read all the docs.
                In the end, Material-UI (as most comprehansive libs) is very capable, but it takes a bit of time to learn how they do things.
                <br /><br />
                For example, they have very powerful theming apis which I discovered too late to implement them here.
                <br /><br />
            </Typography>

            <Typography variant="h5" component="h2" gutterBottom={true} >
                What could be better
            </Typography>

            <Typography variant="body1" >
                I'm not happy with the styling of the Topic select. It's too far off from the brief and I was having too much fine-tuning that Material-UI select.
                That componend looks rushed and is too messy just because I ran out of time with all the Material-UI tweaking.
                <br /><br />
            </Typography>

            <Typography variant="body1" >
                I underestimated how much time learning and customising Material UI would take.
                As a result, the last components I did here (the Post Settings in the side nav of the Mobile page) are messy and rushed.
                I would definitely like to clean those up and seperate them into more components and styles.
                <br /><br />
            </Typography>
            <Typography variant="body1" >
                The layout is not responsive, which it should be (particularly the tabs in the side nav bug me) - I thought I'd have enough time to polish those details.
                <br /><br />
            </Typography>
            <Typography variant="body1" >
                Also, the files state, and the Post Setting stare, are in their own components. This shouldn't be so - I was going to push that up to the PageMobile component but just ran out of time.
            </Typography>
        </div>
    )
}


export default AppContent;