import React from 'react';
import FileUploadMedia from 'components/utils/FileUploads/FileUploadMedia';
import FileUpload from 'components/utils/FileUploads/FileUpload';
import PostSettings from './PostSettings'

const Content = () => {

    return (
        <aside className="app-content-wrapper__sidebar">
            <div className="app-content-wrapper__sidebar__section">
                <FileUploadMedia />
            </div>
            <div  className="app-content-wrapper__sidebar__section">
                <FileUpload />
            </div>

            <div  className="app-content-wrapper__sidebar__section">
                <PostSettings />
            </div>


            <div></div>
        </aside>
    )
}


export default Content;