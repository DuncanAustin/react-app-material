/*

This is a mess! I ran out of time with all the Material-UI gotchas.
This needs to be refactored and cleaned up
*/

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Tabs, Tab, FormControlLabel, RadioGroup, Radio, Select, MenuItem, FormControl, InputLabel, Typography } from '@material-ui/core';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUsers, faBell, faComment, faBullhorn } from '@fortawesome/free-solid-svg-icons'


const useStyles = makeStyles({

    root: {
        '& .MuiTab-root': {
            minWidth: 'auto !important',
            flexShrink: 0
        },
        '& .MuiTab-root svg': {
            fontSize: '24px'
        },
        '& .MuiTab-wrapper': {
            textTransform: 'capitalize'
        },
        flexGrow: 1,
        maxWidth: '100%',
    },
    select: {
        '& .MuiSelect-root': {
            padding: '4px'
        },
        '& .followers': {
            fontSize: '0.5em',
            opacity: '0.5'
        },
        '& .MuiSelect-select:focus': {
            backgroundColor: 'transparent'
        },
        '& .MuiInputLabel-root': {
            padding: '4px'
        },
        '&.MuiFormControl-root': {
            borderRadius: '4px',
            backgroundColor: 'rgba(0, 3, 79, 0.05)',
        }
    }
});

export default function PostSettings() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [audience, setAudience] = React.useState('everyone');
    const [topic, setTopic] = React.useState('marketing');

    const handleAudienceChange = (event) => {
        setAudience(event.target.value);
    };

    const handleTopicChange = (event) => {
        console.log(event.target.value)

        setTopic(event.target.value);
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };



    const TabPanel = (props) => {
        const { children, value, index, ...other } = props;

        return (
        <div
            className="app-content-wrapper__sidebar__tab-panel"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <div>{children}</div>
            )}
        </div>
        );
    }

    const followers = (num) => {
        // these styles are a hack... ran out of times
        return <span style={{
            fontSize: '0.75em',
            opacity: 0.5,
            position: 'absolute',
            right: '20px'
        }}>{num} followers</span>
    }


  return (
    <div  className="post-settings">
        <div className={classes.root}>
            <Tabs className={classes.root}
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                aria-label="icon tabs example"
                indicatorColor="primary"
                textColor="primary"
            >
                <Tab icon={<FontAwesomeIcon icon={faUsers} />} aria-label="Audience" label="Audience" />
                <Tab icon={<FontAwesomeIcon icon={faBell} />} aria-label="Notifications" label="Notifications" />
                <Tab icon={<FontAwesomeIcon icon={faComment} />} aria-label="Social" label="Social" />
                <Tab icon={<FontAwesomeIcon icon={faBullhorn} />} aria-label="Campaigns" label="Campaigns" />
            </Tabs>

            <TabPanel value={value} index={0} >
                <FormControl fullWidth>
                    <RadioGroup aria-label="Audience" name="audience" value={audience} onChange={handleAudienceChange} >
                        <FormControlLabel valrian="caption" color="secondary" value="everyone"
                            control={<Radio color="primary" size="small" />}
                            label={<Typography variant="body2">
                                Everyone
                            </Typography>} />
                        <FormControlLabel value="group" control={<Radio size="small" color="primary" />}
                            label={<Typography variant="body2">
                            Group
                        </Typography>} />
                        <FormControlLabel value="topic" control={<Radio color="primary" size="small" />}
                            label={<Typography variant="body2">
                            Topic
                        </Typography>}/>
                    </RadioGroup>
                </FormControl >
                <FormControl fullWidth className={classes.select}>
                    <InputLabel id="post-create-topics">Topic</InputLabel>
                    <Select
                        labelId="post-create-topics"
                        id="demo-simple-select"
                        value={topic}
                        onChange={handleTopicChange}
                    >
                        <MenuItem value="marketing"></MenuItem>
                        <MenuItem value="marketing">Marketing Update {followers("139K")}</MenuItem>
                        <MenuItem value="ceo">CEO Update {followers("139k")}</MenuItem>
                        <MenuItem value="social">Sports and Social {followers("54k")}</MenuItem>
                        <MenuItem value="events">Events {followers("32k")}</MenuItem>
                        <MenuItem value="roving">Roving Recognitions {followers("91k")}</MenuItem>
                        <MenuItem value="starwars">Star Wars {followers("20k")}</MenuItem>
                        <MenuItem value="schmenges">Schmenge Bros {followers("143mil")}</MenuItem>
                    </Select>
                </FormControl >
            </TabPanel>
            <TabPanel value={value} index={2}>
            </TabPanel>
            <TabPanel value={value} index={3}>
            </TabPanel>
            <TabPanel value={value} index={4}>
            </TabPanel>
        </div>
    </div>
  );
}