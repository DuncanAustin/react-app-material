import React from 'react';

import FormControl from '@material-ui/core/FormControl';

import { default as Button } from 'components/utils/InputButton';
import InputText from 'components/utils/InputText';
import InputCheckbox from 'components/utils/InputCheckbox';

const MobileContent = ( { post, onUpdateText, onUpdateCheckbox, isDisabled }) => {
    const inputProps = field => (
        { "data-field": field }
    );

    return (
        <div className="app-content-wrapper__content">

            <FormControl fullWidth>

                <InputText
                    label = "Title"
                    inputProps = { inputProps("title") }
                    error = { post.title.error }
                    onChange = { onUpdateText }
                    value = { post.title.value }
                    helperText = { post.title.helperText }>
                </InputText>

                <InputText
                    label = "Author"
                    inputProps = { inputProps("author") }
                    value = { post.author.value }
                    onChange = { onUpdateText }
                    helperText = { post.author.helperText }
                    error = { post.author.error }>
                </InputText>

                <InputCheckbox
                    inputProps = { inputProps("featured") }
                    checked={ post.featured.value }
                    name="featured"
                    label="Featured Post"
                    onChange = { onUpdateCheckbox }/>

                <InputText
                    label = "Post Body"
                    inputProps = { inputProps("body") }
                    value = { post.body.value }
                    multiline = { true }
                    onChange = { onUpdateText }
                    rows={ 20 }>
                </InputText>

                <div className="flex flex--justify-content-end">
                    <Button variant="contained">
                        Save Draft
                    </Button>

                    <Button variant="contained" color="primary" disabled={isDisabled()}>
                        Publish Now
                    </Button>
                </div>

            </FormControl>
        </div>
    )
}


export default MobileContent;