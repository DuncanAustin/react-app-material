import React from 'react';
import MobileContent from './MobileContent';
import MobileSidebar from './MobileSidebar';
import TypoGraphy from '@material-ui/core/Typography'

class PageHome extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            post: {
                title: {
                    value: "",
                    helperText: "0/124",
                    error: false,
                    maxLength: 120
                },
                author: {
                    value: "",
                    helperText: "0/60",
                    error: false,
                    maxLength: 60
                },
                body: {
                    value: "",
                    helperText: "",
                    error: false
                },
                featured : {
                    value: false,
                    error: false
                }
            }
        };

        this.onUpdateText = this.onUpdateText.bind(this);
        this.onUpdateCheckbox = this.onUpdateCheckbox.bind(this);
        this.setPostField = this.setPostField.bind(this);
        this.isError = this.isError.bind(this);
    }

    setPostField(field, value, helperText = null, error = null) {

        this.setState( prevState => ({
            post: {
                ...prevState.post,
                [field]: {
                    ...prevState.post[field],
                    value: value,
                    helperText : helperText,
                    error
                }
            },
        }));
    }

    isError() {
        return Object.values(this.state.post).reduce((acc, f) => acc || f.error, false);
    }

    onUpdateText( event ) {
        event.persist();
        const { target } = event;
        const field = target.dataset.field;
        const { post } = this.state;

        if (!field) {
            console.warn('Field is not defined for Create New Post InputText', field);
            return ;
        }

        const helperText = `${target.value.length}/${post[field].maxLength}`;
        const error = post[field].maxLength && target.value.length >= post[field].maxLength;

        this.setPostField(field, target.value, helperText, error);
    }

    onUpdateCheckbox( event ) {
        event.persist();
        const { target } = event;
        const field = target.dataset.field;


        if (!field) {
            console.warn('Field is not defined for Create New Post InputCheckbox', field);
            return ;
        }

        this.setPostField(field, target.checked);
    }



    render() {
        const { post } = this.state;

        return [
            <TypoGraphy key="pageTitle" variant="h4">
                    Create New Post
                </TypoGraphy>,
            <div key="pageContentWrapper" className="app-content-wrapper">
                <MobileContent
                    key = "page-mobile-content"
                    post = { post }
                    onUpdateText = { this.onUpdateText }
                    onUpdateCheckbox = { this.onUpdateCheckbox }
                    isDisabled={this.isError}
                    />
                <MobileSidebar  key="page-mobile-sidebar" />
            </div>
        ]

    }
}


export default PageHome