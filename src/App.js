import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";import Header from './components/layout/header/Header'

import AppBody from './components/layout/body/AppBody'


import './App.scss';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <AppBody />
      </Router>
    </div>
  );
}

export default App;
